# And here are a few pages that give you some pointers ON figuring out the basics of creating a channel
# 'http://devblog.plexapp.com/2011/11/16/a-beginners-guide-to-v2-1/'
# 'http://forums.plexapp.com/index.php/topic/28084-plex-plugin-development-walkthrough/'

# The title of your channel should be unique and as explanatory as possible.  The preifx is used for the channel
# store and shows you where the channel is executed in the log files

# import Shared Service Code
Domain                      = SharedCodeService.domain
Headers                     = SharedCodeService.headers.Headers
Common                      = SharedCodeService.common
Metadata                    = SharedCodeService.metadata
Data                       = SharedCodeService.data.Data



TITLE                       = 'AnimeNetwork'
PREFIX                      = '/video/animenetwork'
URL_CACHE_DIR               = 'cacheHTTP'
THUMB_CACHE_DIR             = 'cacheCovers'
BOOKMARK_CACHE_DIR          = 'cacheBookmarks'
TIMEOUT                     = Common.TIMEOUT

# The images below are the default graphic for your channel and should be saved or located in you Resources folder
# The art and icon should be a certain size for channel submission. The graphics should be good quality and not be blurry
# or pixelated. Icons must be 512x512 PNG files and be named, icon-default.png. The art must be 1280x720 JPG files and be
# named, art-default.jpg. The art shows up in the background of the PMC Client, so you want to make sure image you choose 
# is not too busy or distracting.  I tested out a few in PMC to figure out which one looked best.

ART                         = 'art-default.jpg'
ICON                        = 'logo.png'
BOOKMARK_ICON               = 'icon-bookmark.png'
BOOKMARK_ADD_ICON           = 'icon-add-bookmark.png'
BOOKMARK_REMOVE_ICON        = 'icon-remove-bookmark.png'
BOOKMARK_CLEAR_ICON         = 'icon-clear-bookmarks.png'
NEXT_ICON                   = 'icon-next.png'

CFTest_KEY                  = 'Gogoanime'

CP_DATE = ['Plex for Android', 'Plex for iOS', 'Plex Home Theater', 'OpenPHT']


import messages
import requests
from io import open
import rhtml as RHTML
from DevTools import add_dev_tools, SaveCoverImage, SetUpCFTest, ClearCache, BookmarkTools

MC = messages.NewMessageContainer(PREFIX, TITLE)

###################################################################################################
# This (optional) function is initially called by the PMS framework to initialize the plug-in. This includes setting up
# the Plug-in static instance along with the displayed artwork. These setting below are pretty standard
# You first set up the containers and default for all possible objects.  You will probably mostly use Directory Objects
# and Videos Objects. But many of the other objects give you added entry fields you may want to use and set default thumb
# and art for. For a full list of objects and their parameters, refer to the Framework Documentation.
  
def Start():
	Log ('Start()')
	#    HTTP.CacheTime = CACHE_1HOUR
	# This setting a global cache time for all HTTP requests made by the plugin. This over-rides the framework's default 
	# cache period which,
	# I don't remember off the top of my head. It is entirely optional, but if you're going to use it, the idea is to pick a 
	# cache-time that is reasonable. Ie. store data for a long enough time that you can realistically reduce the load on
	# external servers as well as speed up the load-time for HTTP-requests, but not so long that changes/additions are not
	# caught in a reasonable time frame. IMO, unless you specifically want/need a specific cache-length, I would remove that
	# line and allow the framework to manage the cache with its default settings.
	#
	#   HTTP.Headers['User-Agent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:18.0) Gecko/20100101 Firefox/18.0'
	# This assigning a specific User-Agent header for all HTTP requests made by the plugin. Generally speaking, each time a
	# plugin is started, a user-agent is randomly selected from a list to be used for all HTTP requests. In some cases, a
	# plugin will perform better using a specific user-agent instead of a randomly assigned one. For example, some websites
	# return different data for Safari on an iPad, then what they return for Chrome or Firefox on a PC. Again, if you don't
	# have a specific need to set a specific user-agent, I would remove that code from your channel.

	# You set up the default attributes for all you object containers and objects.  You will probably mostly use Directory
	# Objects and Videos Objects but many of the other objects give you added entry fields you may want to use.  For a full 
	# list of objects and their parameters, refer to the Framework Documentation.
	
	HTTP.CacheTime = 0
	HTTP.Headers['User-Agent'] = Common.USER_AGENT
	version = get_channel_version()

	# setup test for cfscrape
	SetUpCFTest(CFTest_KEY)
	
	# setup auto-managed bookmark backups
	BookmarkTools.auto_backup()

	# Important Note: (R stands for Resources folder) to tell the channel where these images are located.

	ObjectContainer.title1 = TITLE
	ObjectContainer.art = R(ART)

	DirectoryObject.thumb = R(ICON)
	DirectoryObject.art = R(ART)
	EpisodeObject.thumb = R(ICON)
	EpisodeObject.art = R(ART)
	VideoClipObject.thumb = R(ICON)
	VideoClipObject.art = R(ART)
	
	Dict['current_ch_version'] = version

###################################################################################################
# This tells Plex how to list you in the available channels and what type of channels this is 
@handler(PREFIX, TITLE, art=ART, thumb=ICON)

# This function is the first and main menu for you channel.
def MainMenu():
	Log('*' * 80)
	Log('* Client.Product         = {}'.format(Client.Product))
	Log('* Client.Platform        = {}'.format(Client.Platform))
	Log('* Client.Version         = {}'.format(Client.Version))
	
	domains = Domain.get_domain_dict()
	
	Log('* domains available {}'.format(str(domains)))
	
	# You have to open an object container to produce the icons you want to appear on this page. 
	oc = ObjectContainer()
	
	cover_url = "https://babyanime.com/assets/images/logo.png"
	cover = Callback(GetThumb, cover_url=cover_url, cover_file=cover_url.rsplit('/')[-1],type_title="Babyanime")

	oc.add(DirectoryObject(
		key=Callback(BabyAnime, title="BabyAnime", url=domains["babyanime"]),
		title='BabyAnime',
		thumb=cover,
		)
	)
	
	cover_url = "https://ww1.gogoanime.io/img/icon/logo.png"
	cover = Callback(GetThumb, cover_url=cover_url, cover_file=cover_url.rsplit('/')[-1],type_title="Gogoanime")
	
	oc.add(DirectoryObject(
		key=Callback(GogoAnime, title="GogoAnime", url=domains["gogoanime"]),
		title='GogoAnime',
		thumb=cover,
		)
	)


	return oc


#######################################################################################################################
@route(PREFIX + '/babyanime')
def BabyAnime(url, title):
	Log ('BabyAnime()')
	oc = ObjectContainer(title2 = title)
	oc.add(DirectoryObject(
		key=Callback(BabyAnimeAlphabetList, url=url, title=title, art=R(ART)), title='Alphabets'))

	status = Dict['Bookmark_Deleted']
	status = {}
	status["bool"] = False
	Log ("* status {}".format(status))
	bookmark_thumb = R(BOOKMARK_ICON)
	oc.add(DirectoryObject(
		key=Callback(BookmarksMain, title='My Bookmarks', status=status), title='My Bookmarks',
		thumb=bookmark_thumb
		))

	return oc
	
#######################################################################################################################	
@route(PREFIX + '/ba-alphabets')
def BabyAnimeAlphabetList(url, title, art):
	"""Create ABC Directory for each Anime site"""

	oc = ObjectContainer(title2='{} By #, A-Z'.format(title), art=art)
	for pname in list('#'+String.UPPERCASE):
		oc.add(DirectoryObject(
			key=Callback(BabyAnimeDirectoryList,
				page=1, pname=pname.lower() if not pname == '#' else '0',
				category=pname, base_url=url, type_title=title, art=art),
			title=pname))
	Log('* Built #, A-Z... Directories')
	return oc

#############################################################################################################################
@route(PREFIX + '/ba-directory')
def BabyAnimeDirectoryList(page, pname, category, base_url, type_title, art):

	main_title = '{} | {} | Page {}'.format(type_title, category, page)
	Log ("* BabyAnimeDirectoryList() {}".format(main_title))
	

	def process_page(page_nr,letter):
		if letter:
				if letter.lower() in ("#"):
						n = 0
				else:
						n = ord(letter.lower()) - 96
				list_url = base_url + "/AnimeList/Letters/"+str(page_nr)+"?c="+str(n)
		else:
				list_url = base_url + "/AnimeList/Letters/"+str(page_nr)

		Log ("Fetching page {}".format(list_url))
		tree = RHTML.ElementFromURL(list_url)
		page_last = page_nr
		try:
				table = tree.xpath('.//*[@id="animetable"]/table')
				tds = table[0].xpath(".//tr/td")

				try:
						page_first = int(tree.xpath('.//*[@class="pagination"]/li/a')[0].get("data-ci-pagination-page"))
				except:
						page_first = page_nr
				try:
						page_last = int(tree.xpath('.//*[@class="pagination"]/li/a')[-2].get("data-ci-pagination-page"))
				except:
						page_last = None

				#print  "Pagination: Starter Page",page_first,"Next Page",page_last

				allseries = []
				serie = {}

				info = {}
				for td in tds:
						#value = td.xpath(".//text()")[0]
						try:
							tooltip = td.get("title")
							tree = HTML.ElementFromString(tooltip)
							cover = tree.xpath('//img')[0].get("src")
							info["cover"] = cover
							summary = tree.xpath('.//*[@class="summary"]/text()')[0]
							info["summary"] = summary
							alt_name = tree.xpath('.//*[@class="name"]/text()')[0]
							info["alternative_name"] = [alt_name]
						except Exception as e:
							#Log ("* No tooltip ({})".format(str(e)))
							tooltip = None

						try:
							value = td[0].text
						except:
							value = td.text

						try:
							#print td,etree.tostring(td)
							a = td.xpath(".//a")
							#print a[0], etree.tostring(a[0])
							href = a[0].get("href")
							if "/Anime/" in href:
								serie["watch_url"]=href
						except Exception as e:
							#print e
							href = None
						#print value

						if "name" not in serie.keys():
							serie["name"] = value
						elif "total_eps" not in serie.keys():
							eps = [int(s) for s in value.split() if s.isdigit()]
							try:
								serie["total_eps"] = eps[0]
							except Exception as e:
								Log ("warning: missing episode info for {}".format(serie["name"]))
								serie["total_eps"] = 0
						elif "year" not in serie.keys():
							year = int(value)
							if year < 1970:
								year = 0
							serie["year"] = year
						elif "status" not in serie.keys():
							serie["status"] = value
						if len(serie.keys()) == 5:
							if not info:
								Log ("warning: missing info for {}".format(serie["name"]))
							serie["info"] = info
							#Log ("* Fetched Serie: {}".format(str(serie)))
							info = {}
							allseries.append(serie)
							serie = {}
				return allseries,page_last
				#return {"page":page_nr,"series":allseries}
		except Exception as e:
				Log ("warning".format(e))
				return [],page_last
				
				
	results,next_page =  process_page(page,pname)
	
	oc = ObjectContainer(title2=main_title, art=art)
	
	Log ("* Processing {} results".format(len(results)))
	for result in results:
		item_info = {
			'item_sys_name': Common.StringCode(string=result["name"], code='encode'),
			'item_title': Common.StringCode(string=result["name"], code='decode'),
			'short_summary': result["info"]["summary"] if "summary" in result["info"].keys() else "No summary",
			'cover_url': result["info"]["cover"],
			'cover_file': result["info"]["cover"].rsplit('/')[-1],
			'type_title': "Babyanime",
			'base_url': base_url,
			'page_url': result["watch_url"],
			'art': art,
			'status': result["status"],
			'year': result["year"],
		}
		cover = Callback(GetThumb, cover_url=item_info["cover_url"], cover_file=item_info["cover_file"], type_title="Babyanime")
		view_title = u'{} | {}'.format(item_info["item_title"], result["status"])
		oc.add(DirectoryObject(
			key=Callback(ItemPage, item_info=item_info),
			title=view_title, summary=item_info["short_summary"], thumb=cover, art=cover))
	
	if int(page) > next_page:
		Log ("* No more next pages == {}".format(next_page))
	else:
		page = int(page) + 1
		Log ('* NextPage          = {}'.format(page))
		oc.add(NextPageObject(
			key=Callback(BabyAnimeDirectoryList,
				page=page, pname=pname, category=category,
				base_url=base_url, type_title=type_title, art=art),
			title='Next Page>>', thumb=R(NEXT_ICON)))
	
	if len(oc) > 0:
		Dict.Save()
		return oc
		
	return MC.message_container(type_title, '{} list is empty'.format(category))
#######################################################################################################################
@route(PREFIX + '/gogoanime')
def GogoAnime(url, title):
	Log ('GogoAnime()')
	oc = ObjectContainer(title2 = title)
	oc.add(DirectoryObject(
		key=Callback(BabyAnimeAlphabetList, url=url, title=title, art=R(ART)), title='Alphabets'))

	status = Dict['Bookmark_Deleted']
	bookmark_thumb = R(BOOKMARK_ICON)
	oc.add(DirectoryObject(
		key=Callback(BookmarksMain, title='My Bookmarks', status=status), title='My Bookmarks',
		thumb=bookmark_thumb
		))

	return oc
#######################################################################################################################	
@route(PREFIX + '/gogo-alphabets')
def GogoAnimeAlphabetList(url, title, art):
	"""Create ABC Directory for each Anime site"""

	oc = ObjectContainer(title2='{} By #, A-Z'.format(title), art=art)
	for pname in list('#'+String.UPPERCASE):
		oc.add(DirectoryObject(
			key=Callback(GogoAnimeDirectoryList,
				page=1, pname=pname.lower() if not pname == '#' else '0',
				category=pname, base_url=url, type_title=title, art=art),
			title=pname))
	Log('* Built #, A-Z... Directories')
	return oc
#############################################################################################################################
@route(PREFIX + '/gogo-directory')
def GogoAnimeDirectoryList(page, pname, category, base_url, type_title, art):


	main_title = '{} | {} | Page {}'.format(type_title, category, page)
	
	oc = ObjectContainer(title2=main_title, art=R(ART))
	if len(oc) > 0:
		Dict.Save()
		return oc
	return MC.message_container(type_title, '{} list is empty'.format(category))
#############################################################################################################################
@route(PREFIX + '/item', item_info=dict)
def ItemPage(item_info):
	"""Create the Media Page with the Video(s)/Chapter(s) section and a Bookmark option Add/Remove"""

	# set variables
	item_sys_name = item_info['item_sys_name']
	item_title = item_info['item_title']
	type_title = item_info['type_title']
	base_url = item_info['base_url']
	page_url = item_info['base_url'] + item_info['page_url']
	art = item_info['art']
	status = item_info['status']
	year = item_info['year']

	# decode string & set title2 for oc
	item_title_decode = unicode(Common.StringCode(string=item_title, code='decode'))
	title2 = u'{} | {}'.format(type_title, item_title_decode)
	
	Log ("* ItemPage() {}".format(title2))
	Log ("* ItemPage() {}".format(art))

	Log ("* Fetching {}".format(page_url))
	html = RHTML.ElementFromURL(page_url)

	# page category stings depending on media
	page_category = 'Video(s)'

	# update item_info to include page_category
	item_info.update({'page_category': page_category})

	# format item_url for parsing
	Log('* page url = {}'.format(page_url))
	Log('* base url = {}'.format(base_url))
	Log('*' * 80)

	cover = Callback(GetThumb, cover_url=item_info['cover_url'], cover_file=item_info['cover_file'],type_title=type_title)

	show_info = {
					'tv_show_name': item_info["item_title"], 'summary': item_info["short_summary"],
					'source_title': 'Babyanime','tags': None,
					'year': item_info["year"], 'countries': None }
	summary2 = item_info["short_summary"]

	oc = ObjectContainer(title2=title2, art=R(ART))
	
	oc.add(TVShowObject(
		key=Callback(ShowSubPage, item_info=item_info, show_info=show_info),
		rating_key=page_url, title=show_info['tv_show_name'],
		source_title=show_info['source_title'], summary=unicode(summary2),
		thumb=cover, art=R(ART)
		))

	# Test if the Dict does have the 'Bookmarks' section
	bm = Dict['Bookmarks']
	if ((True if [b[type_title] for b in bm[type_title] if b[type_title] == item_sys_name] else False) if type_title in bm.keys() else False) if bm else False:
		# provide a way to remove Item from bookmarks list
		oc.add(DirectoryObject(
			key=Callback(RemoveBookmark, item_info=item_info),
			title='Remove Bookmark', thumb=R(BOOKMARK_REMOVE_ICON),
			summary=u'Remove \"{}\" from your Bookmarks list.'.format(item_title_decode)))
	else:
		# Item not in 'Bookmarks' yet, so lets parse it for adding!
		oc.add(DirectoryObject(
			key=Callback(AddBookmark, item_info=item_info),
			title='Add Bookmark', thumb=R(BOOKMARK_ADD_ICON),
			summary=u'Add \"{}\" to your Bookmarks list.'.format(item_title_decode)))

	return oc
####################################################################################################
@route(PREFIX + '/show-sub-page', item_info=dict, show_info=dict)
def ShowSubPage(item_info, show_info):
	"""Setup Show page"""

	item_title_decode = Common.StringCode(string=item_info['item_title'], code='decode')
	title2 = u'{} | {} | {}'.format(item_info['type_title'], item_title_decode, item_info['page_category'].lower())
	
	Log ("* ShowSubPage() {}".format(title2))
	oc = ObjectContainer(title2=title2, art=R(ART))

	ep_list = []
	Log ("* Fetching {}".format(item_info['base_url'] + item_info['page_url']))
	html = RHTML.ElementFromURL(item_info['base_url'] + item_info['page_url'])

	def process_ep_list(tree):
		table = tree.xpath('.//*[@id="animeepisode"]/table')[0]
		#print table,etree.tostring(table)
		tds = table.xpath(".//tr/td")
		sinfo = []
		ep = {}
		for td in tds:
				try:
						a = td.xpath(".//a")[0]
						href = a.get("href")
						ep["url"] = item_info['base_url']+href
				except:
						href = None
				value = None
				try:
						value = td[0].text
				except:
						value = td.text
				print value

				if "title" not in ep.keys():
						#print "name"
						ep["title"] ="Episode"+ str(value.split("Episode")[-1])
						continue
				elif "date" not in ep.keys():
						#print "date"
						ep["date"] = value

				#print ep
				sinfo.append(ep)
				ep = {}
		return list(reversed(sinfo))
			
	ep_list = process_ep_list(html)

	if not ep_list:
		return MC.message_container(u'Warning', '{} \"{}\" Not Yet Aired.'.format(item_info['type_title'], item_title_decode))
	else:
		tags = Metadata.string_to_list(Common.StringCode(string=show_info['tags'], code='decode')) if show_info['tags'] else []
		thumb = Callback(GetThumb, cover_url=item_info['cover_url'], cover_file=item_info['cover_file'],type_title="Babyanime")
		summary = unicode(Metadata.GetSummary(html))
		#show_name_raw = html.xpath('//div[@class="barContent"]/div/a[@class="bigChar"]/text()')[0]
		season_dict = None
		main_ep_count = len(ep_list)
		ips = 30
		cp = Client.Product
		season_info = {
			'season': '1', 'ep_count': main_ep_count, 'tv_show_name': show_info['tv_show_name'],
			'art': ART, 'source_title': show_info['source_title'],
			'page_url': item_info['base_url'] + item_info['page_url'], 'cover_url': item_info['cover_url'],
			'cover_file': item_info['cover_file'], 'year': show_info['year'], 'tags': show_info['tags'],
			'item_title': item_info['item_title'], 'type_title': item_info['type_title'], 'ips': str(ips)
			}

		Log('*' * 80)
		Log('* ep_list lenght = {}'.format(main_ep_count))

		for ep in ep_list:
			title_lower = ep['title'].lower()

			if 'episode' in title_lower and 'uncensored' not in title_lower:
				season_number = Metadata.GetSeasonNumber(ep['title'], item_info["item_title"], tags, summary)
				pass
			else:
				season_number = '0'

			if not season_dict:
				season_dict = {season_number: [ep['title']]}
			elif season_number in season_dict.keys():
				season_dict[season_number].append(ep['title'])
			else:
				season_dict.update({season_number: [ep['title']]})

		season_info["ep_list"] = ep_list

		for season in sorted(season_dict.keys()):
			ep_count = len(season_dict[season])
			Log('* ep_count = {}'.format(ep_count))
			s0 = (ep_count if season == '0' else (len(season_dict['0']) if '0' in season_dict.keys() else 0))
			season_info.update({'season': season, 'ep_count': ep_count, 'fseason': season, 'season0': s0})
			ep_info = '' if cp in CP_DATE else ' | {} Episodes'.format(ep_count)
			s0_summary = '{}: S0 Specials, Uncensored Episodes, and Miscellaneous Videos{}'.format(show_info['tv_show_name'], ep_info)
			s_summary = '{}: S{} Episodes{}'.format(show_info['tv_show_name'], season, ep_info)
			if (ep_count > ips) and (season != '0'):
				season = int(season)
				x, r = divmod(main_ep_count-s0, ips)
				nseason_list = [str(t) for t in xrange(season, x + (1 if r > 0 else 0) + season)]
				Log('* new season list = {}'.format(nseason_list))
				for i, nseason in enumerate(nseason_list):
					nep_count = ((ips if r == 0 else r) if i+1 == len(nseason_list) else ips)
					Log('* nep_count = {}'.format(nep_count))
					season_info.update({'season': nseason, 'ep_count': nep_count, 'fseason': str(i+1)})
					nep_info = '' if cp in CP_DATE else ' | {} Episodes'.format(nep_count)
					s_summary = '{}: S{} seperated out S{} into multiple seasons{}'.format(show_info['tv_show_name'], nseason, season, nep_info)
					oc.add(SeasonObject(
						key=Callback(SeasonSubPage, season_info=season_info),
						rating_key=item_info['base_url'] + item_info['page_url'] + nseason,
						title='Season {}'.format(nseason), show=show_info['tv_show_name'],
						index=int(nseason), episode_count=nep_count,
						source_title=show_info['source_title'], thumb=thumb, art=R(ART),
						summary=s_summary
						))
			else:
				oc.add(SeasonObject(
					key=Callback(SeasonSubPage, season_info=season_info),
					rating_key=item_info['base_url'] + item_info['page_url'] + season,
					title='Season {}'.format(season), show=show_info['tv_show_name'],
					index=int(season), episode_count=ep_count,
					source_title=show_info['source_title'], thumb=thumb, art=R(ART),
					summary=s0_summary if season == '0' else s_summary
					))

		Log('*' * 80)
		return oc
####################################################################################################
@route(PREFIX + '/season-sub-page', season_info=dict)
def SeasonSubPage(season_info):
    """Setup Episodes for Season"""

    title2 = u'{} | Season {}'.format(season_info['tv_show_name'], season_info['season'])

    oc = ObjectContainer(title2=title2, art=R(ART))

    html = RHTML.ElementFromURL(season_info['page_url'])

    ep_list = season_info["ep_list"]
    tags = []
    thumb = Callback(GetThumb, cover_url=season_info['cover_url'], cover_file=season_info['cover_file'],type_title="Babyanime")
    summary = "NA"
    show_name_raw = season_info['tv_show_name']
    ips = int(season_info['ips'])
    cp = Client.Product

    ep_list2 = []
    for ep in ep_list:
        ep_name, ep_number = Metadata.GetEpisodeNameAndNumber(html, ep['title'], ep['url'])
        season_number = Metadata.GetSeasonNumber(ep['title'], show_name_raw, tags, summary)
        if season_number == season_info['season']:
            ep.update({'season_number': season_number, 'ep_number': ep_number})
            ep_list2.append(ep)

    if ((len(ep_list2) > ips and season_info['season'] != '0') or (len(ep_list2) == 0)):
        temp = int(season_info['fseason'])
        s0 = int(season_info['season0'])
        if len(ep_list2) == 0:
            nep_list = ep_list[ ( ((temp-1)*ips) + s0 ) : ((temp*ips) + s0) ]
        else:
            nep_list = ep_list2[((temp - 1)*ips):((temp)*ips)]
        for nep in nep_list:
            ep_name, ep_number = Metadata.GetEpisodeNameAndNumber(html, nep['title'], nep['url'])
            season_number = Metadata.GetSeasonNumber(nep['title'], show_name_raw, tags, summary)
            oc.add(EpisodeObject(
                source_title=season_info['source_title'],
                title=nep['title'] if cp in CP_DATE else '{} | {}'.format(nep['title'], (nep['date'] if nep['date'] else 'NA')),
                show=season_info['tv_show_name'],
                season=int(season_number),
                index=int(ep_number),
                thumb=thumb,
                art=R(ART),
                originally_available_at=Datetime.ParseDate(nep['date']) if nep['date'] else None,
                url=nep['url']
                ))
    else:
        for ep in ep_list2:
            oc.add(EpisodeObject(
                source_title=season_info['source_title'],
                title=ep['title'] if cp in CP_DATE else '{} | {}'.format(ep['title'], (ep['date'] if ep['date'] else 'NA')),
                show=season_info['tv_show_name'],
                season=int(ep['season_number']),
                index=int(ep['ep_number']),
                thumb=thumb,
                art=R(ART),
                originally_available_at=Datetime.ParseDate(ep['date']) if ep['date'] else None,
                url=ep['url']
                ))

    return oc
#############################################################################################################################
@route(PREFIX + '/bookmarks', status=dict)
def BookmarksMain(title, status):
	"""Create Bookmark Main Menu"""

	if (status['bool']) and (Client.Platform not in ['Plex Home Theater', 'OpenPHT']):
		oc = ObjectContainer(title2=title, header="My Bookmarks",
			message='{} bookmarks have been cleared.'.format(status['type_title']), no_cache=True)
	else:
		oc = ObjectContainer(title2=title, no_cache=True)

	# check for 'Bookmarks' section in Dict
	if not Dict['Bookmarks']:
		# if no 'Bookmarks' section the return pop up
		return MC.message_container(title,
			'No Bookmarks yet. Get out there and start adding some!!!')
	# create boomark directory based on category
	else:
		key_list = sorted(Dict['Bookmarks'].keys())
		Log ('* Bookmarks keys {}'.format(key_list))
		'''
		# return bookmark list directly if only one kiss site selected in prefs
		bm_prefs_names = [('kissasian' if m == 'Drama' else 'kiss{}'.format(m.lower())) for m in key_list]
		bool_prefs_names = [p for p in bm_prefs_names if Prefs[p]]
		if len(bool_prefs_names) == 1:
			b_prefs_name = bool_prefs_names[0].split('kiss')[1].title()
			b_prefs_name = 'Drama' if b_prefs_name == 'Asian' else b_prefs_name
			if b_prefs_name in key_list:
				art = 'art-{}.jpg'.format(b_prefs_name.lower())
				return BookmarksSub(b_prefs_name, art)
		'''
		return BookmarksSub("Babyanime", R(ART))
		# list categories in bookmarks dictionary that are selected in prefs
		for key in key_list:
			prefs_name = 'kissasian' if key == 'Drama' else 'kiss{}'.format(key.lower())
			art = 'art-{}.jpg'.format(key.lower())
			thumb = 'icon-{}.png'.format(key.lower())

			# if site in Prefs then add its bookmark section
			if Prefs[prefs_name]:
				# Create sub Categories for Anime, Cartoon, Drama, and Manga
				oc.add(DirectoryObject(
					key=Callback(BookmarksSub, type_title=key, art=art),
					title=key, thumb=R(thumb), summary='Display {} Bookmarks'.format(key), art=R(art)))

		# test if no sites are picked in the Prefs
		if len(oc) > 0:
			# hide/unhide clear bookmarks option, from DevTools
			if not Prefs['hide_clear_bookmarks']:
				# add a way to clear the entire bookmarks list, i.e. start fresh
				oc.add(DirectoryObject(
					key=Callback(ClearBookmarks, type_title='All'),
					title='Clear All Bookmarks?',
					thumb=R(BOOKMARK_CLEAR_ICON), art=R(ART),
					summary='CAUTION! This will clear your entire bookmark list, even those hidden!'))

			return oc
		else:
			# Give error message
			return MC.message_container('Error',
				'At least one source must be selected in Preferences to view Bookmarks')
####################################################################################################
@route(PREFIX + '/bookmarkssub')
def BookmarksSub(type_title, art):
	"""Load Bookmarked items from Dict"""

	if not type_title in Dict['Bookmarks'].keys():
		return MC.message_container('Error',
			'{} Bookmarks list is dirty. Use About/Help > Dev Tools > Bookmark Tools > Reset {} Bookmarks'.format(type_title, type_title))

	oc = ObjectContainer(title2='My Bookmarks | {}'.format(type_title), art=R(ART))

	# Fill in DirectoryObject information from the bookmark list
	# create empty list for testing covers
	#for bookmark in sorted(Dict['Bookmarks'][type_title], key=lambda k: k[type_title]):
	for bookmark in Util.ListSortedByKey(Dict['Bookmarks'][type_title], type_title):
		item_title = bookmark['item_title']
		summary = bookmark['summary']
		summary2 = Common.StringCode(string=summary, code='decode') if summary else None

		cover_file = Common.CorrectCoverImage(bookmark['cover_file']) if bookmark['cover_file'] else None
		cover_url = Common.CorrectCoverImage(bookmark['cover_url']) if bookmark['cover_url'] else None

		cover = None
		if cover_file and cover_url:
			cover = Callback(GetThumb, cover_url=cover_url, cover_file=cover_file,type_title=type_title)

		item_info = {
			'item_sys_name': bookmark[type_title],
			'item_title': item_title,
			'short_summary': summary,
			'year': bookmark["year"],
			'summary': summary,
			'cover_url': cover_url,
			'cover_file': cover_file,
			'type_title': type_title,
			'status': bookmark['status'],
			'base_url': bookmark['base_url'],
			'page_url': bookmark['page_url'],
			'art': art}

		# get genre info, provide legacy support for older Bookmarks
		#   by updating
		if 'genres' in bookmark.keys():
			genres = [g.replace('_', ' ') for g in bookmark['genres'].split()]
			update = False
			if cover_url and Common.is_kiss_url(cover_url):
				cover_type_title = Common.GetTypeTitle(cover_url)
				cover_base_url = Regex(r'(https?\:\/\/(?:www\.|ww1\.|w1\.)?\w+\.\w+)').search(bookmark['cover_url']).group(1)
				if (cover_type_title, cover_base_url) not in Common.BaseURLListTuple():
					Log.Debug('* Bookmark Cover URL Domain changed. Updating Bookmark Metadata.')
					update = True
			else:
				page_base_url = Regex(r'(https?\:\/\/(?:www\.|ww1\.|w1\.)?\w+\.\w+)').search(bookmark['base_url']+bookmark['page_url']).group(1)
				if (type_title, page_base_url) not in Common.BaseURLListTuple():
					Log.Debug('* Bookmark Page URL Domain changed. Updating Bookmark Metadata.')
					update = True
			if update:
				bm_info = item_info.copy()
				bm_info.update({'type_title': type_title})
				ftimer = float(Util.RandomInt(0,30)) + Util.Random()
				Thread.CreateTimer(interval=ftimer, f=UpdateLegacyBookmark, bm_info=bm_info)
		else:
			bm_info = item_info.copy()
			bm_info.update({'type_title': type_title})
			ftimer = float(Util.RandomInt(0,30)) + Util.Random()
			Thread.CreateTimer(interval=ftimer, f=UpdateLegacyBookmark, bm_info=bm_info)
			genres = ['Temp']

		oc.add(DirectoryObject(
			key=Callback(ItemPage, item_info=item_info),
			title=Common.StringCode(string=item_title, code='decode'),
			summary=summary2, thumb=cover, art=cover))

	if not Prefs['hide_clear_bookmarks']:
		# add a way to clear this bookmark section and start fresh
		oc.add(DirectoryObject(
			key=Callback(ClearBookmarks, type_title=type_title),
			title='Clear All \"{}\" Bookmarks?'.format(type_title),
			thumb=R(BOOKMARK_CLEAR_ICON), art=R(ART),
			summary='CAUTION! This will clear your entire \"{}\" bookmark section!'.format(type_title)))

	return oc
####################################################################################################
@route(PREFIX + '/addbookmark', item_info=dict)
def AddBookmark(item_info):
	"""Adds Item to the bookmarks list"""

	# set variables
	item_sys_name = item_info['item_sys_name']
	item_title = item_info['item_title']
	type_title = item_info['type_title']
	cover_url = item_info['cover_url']
	status = item_info['status']
	base_url = item_info['base_url']
	page_url = item_info['page_url']

	# decode title string
	item_title_decode = Common.StringCode(string=item_title, code='decode')
	Log('*' * 80)
	Log(u'* item to add = {} | {}'.format(item_title_decode, item_sys_name))

	# setup html for parsing
	html = RHTML.ElementFromURL(page_url)

	# Genres
	genres = html.xpath('//p[span[@class="info"]="Genres:"]/a/text()')
	if genres:
		genres = ' '.join([g.replace(' ', '_') for g in genres])
	else:
		genres = ''

	# if no cover url then try and find one on the item page
	if cover_url:
		cover_url = Common.CorrectCoverImage(cover_url)
		image_file = item_info['cover_file']
	else:
		try:
			cover_url = Common.CorrectCoverImage(html.xpath('//head/link[@rel="image_src"]')[0].get('href'))
			if not 'http' in cover_url:
				cover_url = None
				image_file = None
			else:
				image_file = cover_url.split('/', 3)[3].replace('/', '_')
		except:
			cover_url = None
			image_file = None

	# get summary
	summary = Metadata.GetSummary(html)

	# setup new bookmark json data to add to Dict
	new_bookmark = {
		type_title: item_sys_name, 'item_title': item_title, 'cover_file': image_file,'status':status,'base_url':base_url,
		'cover_url': cover_url, 'summary': item_info["short_summary"], 'genres': genres, 'page_url': page_url, "year": item_info["year"]}

	Log('* new bookmark to add >>')
	Log(u'* {}'.format(new_bookmark))

	bm = Dict['Bookmarks']

	# Test if the Dict has the 'Bookmarks' section yet
	if not bm:
		# Create new 'Bookmarks' section and fill with the first bookmark
		Dict['Bookmarks'] = {type_title: [new_bookmark]}
		Log('* Inital bookmark list created >>')
		Log(u'* {}'.format(bm))
		Log('*' * 80)

		# Update Dict to include new 'Bookmarks' section
		Dict.Save()

		# Provide feedback that the Item has been added to bookmarks
		return MC.message_container(unicode(item_title_decode),
			u'\"{}\" has been added to your bookmarks.'.format(item_title_decode))
	# check if the category key 'Anime', 'Manga', 'Cartoon', 'Drama', or 'Comic' exist
	# if so then append new bookmark to one of those categories
	elif type_title in bm.keys():
		# fail safe for when clients are out of sync and it trys to add the bookmark in duplicate
		if (True if [b[type_title] for b in bm[type_title] if b[type_title] == item_sys_name] else False):
			# Bookmark already exist, don't add in duplicate
			Log(u'* bookmark \"{}\" already in your bookmarks'.format(item_title_decode))
			Log('*' * 80)
			return MC.message_container(unicode(item_title_decode),
				u'\"{}\" is already in your bookmarks.'.format(item_title_decode))
		# append new bookmark to its correct category, i.e. 'Anime', 'Drama', etc...
		else:
			temp = {}
			temp.setdefault(type_title, bm[type_title]).append(new_bookmark)
			Dict['Bookmarks'][type_title] = temp[type_title]
			Log(u'* bookmark \"{}\" has been appended to your {} bookmarks'.format(item_title_decode, type_title))
			Log('*' * 80)

			# Update Dict to include new Item
			Dict.Save()

			# Provide feedback that the Item has been added to bookmarks
			return MC.message_container(unicode(item_title_decode),
				u'\"{}\" has been added to your bookmarks.'.format(item_title_decode))
	# the category key does not exist yet so create it and fill with new bookmark
	else:
		Dict['Bookmarks'].update({type_title: [new_bookmark]})
		Log(u'* bookmark \"{}\" has been created in new {} section in bookmarks'.format(item_title_decode, type_title))
		Log('*' * 80)

		# Update Dict to include new Item
		Dict.Save()

		# Provide feedback that the Item has been added to bookmarks
		return MC.message_container(unicode(item_title_decode),
			u'\"{}\" has been added to your bookmarks.'.format(item_title_decode))

####################################################################################################
@route(PREFIX + '/removebookmark', item_info=dict)
def RemoveBookmark(item_info):
	"""Removes item from the bookmarks list using the item as a key"""

	# set variables
	item_sys_name = item_info['item_sys_name']
	item_title = item_info['item_title']
	type_title = item_info['type_title']

	# decode string
	item_title_decode = unicode(Common.StringCode(string=item_title, code='decode'))

	# index 'Bookmarks' list
	bm = Dict['Bookmarks'][type_title]
	Log('* bookmark length = {}'.format(len(bm)))
	for i in xrange(len(bm)):
		# remove item's data from 'Bookmarks' list
		if bm[i][type_title] == item_sys_name:
			# if caching covers, then don't remove cover file
			bm.pop(i)
			break

	# update Dict, and debug log
	Dict.Save()
	Log('* \"{}\" has been removed from Bookmark List'.format(item_title_decode))

	if len(bm) == 0:
		# if the last bookmark was removed then clear it's bookmark section
		Log('* {} bookmark was the last, so removed {} bookmark section'.format(item_title_decode, type_title), force=True)
		Log('*' * 80)
		return ClearBookmarksCheck(type_title)
	else:
		Log('*' * 80)
		# Provide feedback that the Item has been removed from the 'Bookmarks' list
		return MC.message_container(type_title,
			'\"{}\" has been removed from your bookmarks.'.format(item_title_decode))
####################################################################################################
@route(PREFIX + '/clearbookmarks')
def ClearBookmarks(type_title):

	art = 'art-{}.jpg'.format(type_title.lower if type_title != 'All' else 'main')
	oc = ObjectContainer(
		title2=u'Clear \'{}\'?'.format(type_title), art=R(ART), no_cache=True
		)
	oc.add(PopupDirectoryObject(
		key=Callback(ClearBookmarksCheck, tt=type_title),
		title='OK?',
		summary='Sure you want to Delete all \'{}\' bookmarks? If NOT then navigate back or away from this page.'.format(type_title),
		thumb=R(BOOKMARK_CLEAR_ICON), art=R(ART)
		))
	return oc
####################################################################################################
@route(PREFIX + '/clearbookmarks-yes')
def ClearBookmarksCheck(tt):
	"""Remove 'Bookmarks' Section(s) from Dict. Note: This removes all bookmarks in list"""

	Log('*' * 80)
	if 'All' in tt:
		# delete 'Bookmarks' section from Dict
		if Dict['Bookmarks']:
			del Dict['Bookmarks']
			Log('* Entire Bookmark Dict Removed')
		else:
			Log('* Entire Bookmark Dict already removed.')
	else:
		# delete section 'Anime', 'Manga', 'Cartoon', 'Drama', or 'Comic' from bookmark list
		if Dict['Bookmarks'] and tt in Dict['Bookmarks'].keys():
			del Dict['Bookmarks'][tt]
			Log('* \"{}\" Bookmark Section Cleared'.format(tt))
		else:
			Log('* \"{}\" Bookmark Section Already Cleared'.format(tt))

	Dict['Bookmark_Deleted'] = {'bool': True, 'type_title': tt}
	status = Dict['Bookmark_Deleted']
	Log('*' * 80)

	Dict.Save()

	# Provide feedback that the correct 'Bookmarks' section is removed
	#   and send back to Bookmark Main Menu
	return BookmarksMain(title='My Bookmarks', status=status)
#############################################################################################################################
# This is a function to pull the thumb from a the head of a page
@route(PREFIX + '/getthumb')
def GetThumb(cover_url, cover_file,type_title=None):

    cover = None
    if not cover_url:
        Log.Error('* No cover_url')
        return cover

    if cover_file:
        if not type_title:
            type_title = Common.GetTypeTitle(cover_url)
        Log('* cover file name   = {}'.format(cover_file))
        if Data.CoverExists(Core.storage.join_path(type_title, cover_file)):
            Log.Debug(u'* Loading cover from {} folder'.format(THUMB_CACHE_DIR))
            cover = Data.data_object(Data.Covers(Core.storage.join_path(type_title, cover_file)))
        else:
            Log ('* Cover not yet saved, saving {} now'.format(cover_file))
            try:
                tt, f = SaveCoverImage(cover_url,type_title)
                cover = Data.data_object(Data.Covers(Core.storage.join_path(tt, f)))
            except:
                Log.Exception(u'* Cannot Save/Load "{}"'.format(cover_url))
                cover = None
    '''
    elif 'http' in cover_url:
        Log('* Thumb NOT hosted on Kiss, Redirecting URL {}'.format(cover_url))
        cover = Redirect(Common.CorrectCoverImage(cover_url))
    '''

    if not cover:
        Log ('* Cover not Cached')
        cover = cover_url
    return cover
####################################################################################################
def get_channel_version():
	plist = Plist.ObjectFromString(Core.storage.load(Core.plist_path))
	return plist['CFBundleVersion'] if 'CFBundleVersion' in plist.keys() else 'Current'